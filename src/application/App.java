package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import entities.Livro;

public class App {
	
	
	
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		// criando livros
		Livro l1 = new Livro("Novos Poemas", "Vinicius de Morais", "poesia", 1938);
		Livro l2 = new Livro("Poemas Escritos na India", "Cecilia Meireles", "poesia", 1962);
		Livro l3 = new Livro("Orfeu da Conceição", "Vinicius de Morais", "teatro", 1954);
		Livro l4 = new Livro("Ariana, a Mulher", "Vinicius de Morais", "poesia", 1936);
		
		List<Livro> livros = new ArrayList<Livro>();
		
		// Inserindo livros no vetor.
		livros.add(l1);
		livros.add(l2);
		livros.add(l3);
		livros.add(l4);
		
		// Mostrando livros ordenados, de acordo com a ordem que foram inseridos no vetor.
		for(Livro l : livros) {
			System.out.println(" INDICE: "+ (livros.indexOf(l)+1) + "\n" + l);
			System.out.println();
		}
		System.out.println();
		
		// Ordenando livros por nome de autor e ano, e mostrando o vetor ordenado.
		Livro.ordenarLivros(livros,livros.size());
		System.out.println("#### Livros Ordenados ####");
		for(Livro l : livros) {
			System.out.println(" INDICE: "+ (livros.indexOf(l)+1) + "\n" + l);
			System.out.println();
		}
		// Pesquisando livros por autor e genero, com base nos dados fornecidos pelo usuario.
		System.out.println("---------PESQUISA DE LIVROS---------");
		System.out.print("Digite o nome do autor: ");
		String autor = sc.nextLine();
		System.out.print("A qual genero pertence o livro: ");
		String genero = sc.nextLine();
		
		System.out.println();
		
		// Mostrando resultado da pesquisa de forma detalhada
		System.out.println("#### Resultado da Pesquisa de Livros ####\n");
		for(Livro l: livros) {
			if(l.buscaLivroModerno(l, autor, genero) != null) {
				System.out.print(" INDICE: "+ (livros.indexOf(l)+1)+"\n");
			}
			System.out.println(l.buscaLivroModerno(l,autor,genero));
			String r = " ";
			
			if(l.buscaLivroModerno(l,autor,genero)!= null) {
				int i = Livro.verificaNoModernismo(l);
				
				switch(i) {
				case -1:
					r = "Pre-Modernismo";
					break;
				case 0:
					r = "Modernismo";
					break;
				case 1:
					r = "Pós-Modernismo";
					break;
				}
				System.out.println(" PERIODO: " + r + "\n");
			}else {
				System.out.println();
			}
			
		}
		sc.close();
	}
}
