package entities;

import java.util.Collections;
import java.util.List;

public class Livro implements Comparable<Livro>{
	private String titulo;
	private String autor;
	private String genero;
	private int ano;
	
	public Livro(String titulo, String autor, String genero, int ano) {
		this.titulo = titulo;
		this.autor = autor;
		this.genero = genero;
		this.ano = ano;
	}
	
	public Livro() {
		
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
	
	public static Integer verificaNoModernismo(Livro livro) {
		Integer x = null;
		if(livro.getAno() < 1930) {
			x = -1;
		}else if((livro.getAno() > 1930) && (livro.getAno() < 1945)) {
			x = 0;
		}else if(livro.getAno() > 1945) {
			x = 1;
		}
		return x;
	}

	public int compareTo(Livro o) {
		
		if(this.getAutor().equals(o.getAutor())) {
			if(this.getAno() < o.getAno()) {
				return -1;
			}
		}
		else {
			return this.getAutor().compareTo(o.getAutor());
		}
		return 0;
	}
	
	public static void ordenarLivros(List<Livro> livros, int totLivros) {
		Collections.sort(livros);
	}
	
	public Livro buscaLivroModerno(Livro livro, String autor, String genero) {
		if(this.getAutor().equals(autor) && this.getGenero().equals(genero)){
			return livro;
		}else {
			return null;
		}	
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return " TITULO: "
				+ this.titulo 
				+ "\n AUTOR: "
				+ this.autor
				+ "\n GENERO: "
				+ this.genero
				+ "\n ANO: "
				+ this.ano 
				+ ".";
	}

}
